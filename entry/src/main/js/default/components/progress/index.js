export default {
    props: {
        percent: {
            default: 0
        },
        strokeWidth: {
            default: '6px'
        }
    },
    data: { },
}

# 进度条组件

### 简介

一个有生命色彩的进度条组件

### 使用说明

1、将组件文件（entry/src/main/js/default/components/progress）copy到开发项目

2、引入组件
```
<element name="progress-component" src="../../components/progress/index.hml"></element>
```

3、使用组件

```
<progress-component percent="50" stroke-width="8"></progress-component>
```



### 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125724_2791d9a2_810819.png "WX20210920-124718@2x.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125737_73f0365e_810819.png "WX20210920-124755@2x.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0920/125748_807a0352_810819.png "WX20210920-124811@2x.png")
